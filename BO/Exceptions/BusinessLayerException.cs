using System;
namespace BO.Exceptions {
    /// <summary>
    /// Useful for defining exceptions in the Business Layer
    /// </summary>
    public class BusinessLayerException : Exception {
        /// <summary>
        /// Default constructor for BusinessLayerException.null Should be avoided in general.
        /// </summary>
        public BusinessLayerException() {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Description of the raised exception passed to the base constructor as parameter.</param>
        /// <returns></returns>
        public BusinessLayerException(string message) : base(message) {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Description of the raised exception passed to the base constructor as parameter.</param>
        /// <param name="innerException">The exception that caused Business Exception to be raised.</param>
        /// <returns></returns>
        public BusinessLayerException(string message, Exception innerException) : base(message, innerException) {
        }
    }
}