using System;
namespace BO.Exceptions {
    /// <summary>
    /// Useful for defining Exceptions in Datalayer.
    /// </summary>
    public class DataLayerException : Exception {
        public DataLayerException() {
        }
        public DataLayerException(string message) : base(message) {
        }
        public DataLayerException(string message, Exception innerException) : base(message, innerException) {
        }
    }
}