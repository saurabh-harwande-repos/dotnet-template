using System;
namespace BO.Exceptions {
    /// <summary>
    /// Raise when there is problem with validation of any business objects or parameters.
    /// </summary>
    public class ValidationException : Exception {
        public ValidationException() {
        }
        public ValidationException(string message) : base(message) {
        }
        public ValidationException(string message, Exception innerException) : base(message, innerException) {
        }
    }
}