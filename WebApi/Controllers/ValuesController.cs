﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BL;
namespace WebApi.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase {
        private IValueLogic _objValueLogic;
        public ValuesController(IValueLogic objValueLogic) {
            _objValueLogic = objValueLogic;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get() {
            IEnumerable<string> allValues = _objValueLogic.GetAll();
            return new ActionResult<IEnumerable<string>>(allValues);
        }
        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id) {
            return _objValueLogic.Get(id);
        }
        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value) {
            _objValueLogic.Insert(value);
        }
        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value) {
            _objValueLogic.Update(id, value);
        }
        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id) {
            _objValueLogic.Delete(id);
        }
    }
}
