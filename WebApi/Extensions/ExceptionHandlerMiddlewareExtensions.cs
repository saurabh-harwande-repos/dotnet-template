using Microsoft.AspNetCore.Builder;
using WebApi.Middleware;

namespace WebApi.Extensions {
    public static class ExceptionHandlerMiddlewareExtensions {
        public static IApplicationBuilder UseErrorHandler(this IApplicationBuilder builder) {
            return builder.UseMiddleware<ExceptionHandlerMiddleware>();
        }
    }
}