using BL;
using DL;
using Microsoft.Extensions.DependencyInjection;

namespace WebApi.Extensions {
    public static class ProjectDependenciesServiceCollectionExtensions {
        public static IServiceCollection AddDependencies(this IServiceCollection services) {
            services.AddTransient<IValueLogic, ValueLogic>();
            services.AddTransient<IValueRepository, ValueRepository>();
            services.AddTransient<IDbConnectionFactory, PostgresConnectionFactory>();
            return services;
        }
    }
}