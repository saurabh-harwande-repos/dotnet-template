using System;
using System.Threading.Tasks;
using BO.Exceptions;
using Microsoft.AspNetCore.Http;

namespace WebApi.Middleware {
    public class ExceptionHandlerMiddleware {
        private readonly RequestDelegate _next;
        public ExceptionHandlerMiddleware(RequestDelegate next) {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext context) {
            try {
                await _next(context);
            }
            catch (DataLayerException ex) {
            }
            catch (BusinessLayerException ex) {
            }
            catch (ValidationException ex) {
            }
            catch (Exception ex) {
            }
        }
    }
}