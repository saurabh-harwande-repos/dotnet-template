using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Xunit;
using Moq;
using BL;
using DL;

namespace Test.BL {
    public class ValuesLogicTest {
        private IValueLogic _objValueLogic;
        public ValuesLogicTest() {
            Mock<IValueRepository> mockValueRepository = new Mock<IValueRepository>();
            mockValueRepository.Setup(x => x.GetAll()).Returns(new string[] {"Value1", "Value2"});
            Mock<ILogger<ValueLogic>> mockLogger = new Mock<ILogger<ValueLogic>>();
            _objValueLogic = new ValueLogic(mockValueRepository.Object, mockLogger.Object);
        }
        [Fact]
        public void GetAll_Success() {
            IEnumerable<string> expected = new string[] {"Value1", "Value2"};
            IEnumerable<string> actual = _objValueLogic.GetAll();
            Assert.Equal<IEnumerable<string>>(expected, actual);
        }
    }
}