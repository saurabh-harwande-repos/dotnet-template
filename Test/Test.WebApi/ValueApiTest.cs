using System;
using System.Collections.Generic;
using Xunit;
using Moq;
using WebApi.Controllers;
using BL;

namespace Test.WebApi
{
    public class ValueApiTest
    {
        private ValuesController _objValuesController;
        public ValueApiTest() {
            Mock<IValueLogic> mockValueLogic = new Mock<IValueLogic>();
            mockValueLogic.Setup(x => x.GetAll()).Returns(new string[] {"Value1", "Value2"});
            _objValuesController = new ValuesController(mockValueLogic.Object);
        }
        [Fact]
        public void GetAll_Success() {
            IEnumerable<string> expected = new String[] {"Value1", "Value2"};
            IEnumerable<string> actual = _objValuesController.Get().Value;
            Assert.Equal<IEnumerable<string>>(expected, actual);
        }
    }
}
