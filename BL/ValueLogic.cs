﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using DL;
using BO.Exceptions;

namespace BL {
    public class ValueLogic : IValueLogic {
        private IValueRepository _objIValueRepository;
        private readonly ILogger _logger;
        public ValueLogic(IValueRepository objIValueRepository, ILogger<ValueLogic> objLogger) {
            _objIValueRepository = objIValueRepository;
            _logger = objLogger;
        }
        IEnumerable<string> IValueLogic.GetAll() {
            try {
                return _objIValueRepository.GetAll();
            }
            catch(DataLayerException ex) {
                throw new BusinessLayerException(ex.Message, ex.InnerException);
            }
            catch(Exception ex) {
                throw new BusinessLayerException("Some Message", ex);
            }
        }
        string IValueLogic.Get(int id) {
            return _objIValueRepository.Get(id);
        }
        void IValueLogic.Insert(string value) {
            _objIValueRepository.Insert(value);
        }
        void IValueLogic.Update(int id, string value) {
            _objIValueRepository.Update(id, value);
        }
        void IValueLogic.Delete(int id) {
            _objIValueRepository.Delete(id);
        }
    }
}
