using System.Collections.Generic;
using DL;

namespace BL {
    public interface IValueLogic {
        IEnumerable<string> GetAll();
        string Get(int id);
        void Insert(string value);
        void Update(int id, string value);
        void Delete(int id);
    }
}