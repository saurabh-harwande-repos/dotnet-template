using System.Collections.Generic;

namespace DL {
    public interface IValueRepository
    {
        IEnumerable<string> GetAll();
        string Get(int id);
        void Insert(string value);
        void Update(int id, string value);
        void Delete(int id);
    }
}