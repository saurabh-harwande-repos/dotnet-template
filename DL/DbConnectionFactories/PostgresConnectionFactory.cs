using System.Data;
using Npgsql;

namespace DL
{
    public class PostgresConnectionFactory : IDbConnectionFactory
    {
        IDbConnection IDbConnectionFactory.CreateConnection(string connectionString) {
            return new NpgsqlConnection(connectionString);
        }
    }
}