﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Dapper;
using BO.Configurations;
using BO.Exceptions;

namespace DL {
    public class ValueRepository : IValueRepository {
        private ConnectionStrings _connectionStrings;
        private IDbConnectionFactory _connectionFactory;
        private readonly ILogger _logger;
        public ValueRepository(IOptionsMonitor<ConnectionStrings> optionsAccessor, IDbConnectionFactory connectionFactory, ILogger<ValueRepository> objLogger) {
            _connectionStrings = optionsAccessor.CurrentValue;
            _connectionFactory = connectionFactory;
            _logger = objLogger;
        }
        IEnumerable<string> IValueRepository.GetAll() {
            IEnumerable<string> result = null;
            try {
                using(IDbConnection connection = _connectionFactory.CreateConnection(_connectionStrings.PostgresConnection)) {
                    connection.Open();
                    result = connection.Query<string>("SELECT \"Name\" FROM \"dbo\".\"Values\"");
                }
            }
            catch(Exception ex) {
                new DataLayerException("Some Message", ex);
            }
            return result;
        }
        string IValueRepository.Get(int id) {
            string result = null;
            using(IDbConnection connection = _connectionFactory.CreateConnection(_connectionStrings.PostgresConnection)) {
                connection.Open();
                result = connection.QueryFirst<string>("SELECT \"Name\" FROM \"dbo\".\"Values\" WHERE \"Id\"=" + id);
            }
            return result;
        }
        void IValueRepository.Insert(string value) {
            using(IDbConnection connection = _connectionFactory.CreateConnection(_connectionStrings.PostgresConnection)) {
                connection.Open();
                connection.Execute("INSERT INTO \"dbo\".\"Values\"(\"Name\") VALUES ('" + value + "')");
            }
        }
        void IValueRepository.Update(int id, string value) {
            using(IDbConnection connection = _connectionFactory.CreateConnection(_connectionStrings.PostgresConnection)) {
                connection.Open();
                connection.Execute("UPDATE \"dbo\".\"Values\" SET \"Name\"='" + value + "' WHERE \"Id\"=" + id);
            }
        }
        void IValueRepository.Delete(int id) {
            using(IDbConnection connection = _connectionFactory.CreateConnection(_connectionStrings.PostgresConnection)) {
                connection.Open();
                connection.Execute("DELETE FROM \"dbo\".\"Values\" WHERE \"Id\"=" + id);
            }
        }
    }
}
